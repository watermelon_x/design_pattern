package behavior_patterns.state_pattern;

/**
 * 状态模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("-----------初始化游戏-------------------");
        CurrentState start = new CurrentState();
        start.printIntegral();

        System.out.println("-------------吃到一个苹果------------------");
        start.eatApple();
        start.printIntegral();

        System.out.println("-------------又吃到一个苹果------------------");
        start.eatApple();
        start.printIntegral();

        System.out.println("-------------吃到一个毒苹果------------------");
        start.eatPoisonousApple();
        start.printIntegral();

        System.out.println("-------------吃到一个无敌苹果------------------");
        start.eatInvincibleApple();
        start.printIntegral();

        System.out.println("-------------吃到一个无敌苹果之后吃到一个苹果------------------");
        start.eatApple();
        start.printIntegral();

        System.out.println("-------------吃到一个无敌苹果之后吃到一个毒苹果------------------");
        start.eatPoisonousApple();
        start.printIntegral();

        System.out.println("-------------吃到一个无敌苹果之后撞到障碍物------------------");
        start.TouchAnObstacle();
        start.printIntegral();

        try {
            System.out.println("-------------等待3秒无敌时间结束-------------");
            Thread.sleep(3000);
            System.out.println("-------------无敌时间已结束-------------");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("-------------无敌时间过期之后,吃到一个毒苹果------------------");
        start.eatPoisonousApple();
        start.printIntegral();


        System.out.println("-------------无敌时间过期之后,撞到障碍物------------------");
        start.TouchAnObstacle();
        start.printIntegral();

        System.out.println("-------------撞到障碍物之后,吃到苹果------------------");
        start.eatApple();
        start.printIntegral();

    }
}
