package behavior_patterns.state_pattern;

import behavior_patterns.state_pattern.state.DeathState;
import behavior_patterns.state_pattern.state.InvincibleState;
import behavior_patterns.state_pattern.state.NormalState;
import behavior_patterns.state_pattern.state.State;

/**
 * 当前状态
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class CurrentState implements State {

    private NormalState normalState;
    private InvincibleState invincibleState;
    private DeathState deathState;

    //当前积分
    private Integer integral = 0;

    //当前状态
    private State state;

    public CurrentState() {
        normalState = new NormalState(this);
        invincibleState = new InvincibleState(this);
        deathState = new DeathState(this);
        setState(normalState);
    }

    @Override
    public void eatApple() {
        this.state.eatApple();
    }

    @Override
    public void eatPoisonousApple() {
        this.state.eatPoisonousApple();
    }

    @Override
    public void eatInvincibleApple() {
        this.state.eatInvincibleApple();
    }

    @Override
    public void TouchAnObstacle() {
        this.state.TouchAnObstacle();
    }

    /**
     * 打印当前积分
     */
    public void printIntegral() {
        System.out.println("当前积分:" + this.getIntegral());
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public NormalState getNormalState() {
        return normalState;
    }

    public void setNormalState(NormalState normalState) {
        this.normalState = normalState;
    }

    public InvincibleState getInvincibleState() {
        return invincibleState;
    }

    public void setInvincibleState(InvincibleState invincibleState) {
        this.invincibleState = invincibleState;
    }

    public DeathState getDeathState() {
        return deathState;
    }

    public void setDeathState(DeathState deathState) {
        this.deathState = deathState;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }
}
