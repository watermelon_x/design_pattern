package behavior_patterns.state_pattern.state;

/**
 * 状态接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public interface State {

    /**
     * 吃到苹果  正常情况下:加积分
     */
    void eatApple();

    /**
     * 吃到毒苹果 正常情况下:减积分
     */
    void eatPoisonousApple();

    /**
     * 吃到增加无敌buff的果实   正常情况下:无敌一段时间
     */
    void eatInvincibleApple();

    /**
     * 触碰障碍物  正常情况下:死亡
     */
    void TouchAnObstacle();

}
