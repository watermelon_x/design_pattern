package behavior_patterns.state_pattern.state;

import behavior_patterns.state_pattern.CurrentState;
import behavior_patterns.state_pattern.Task;

import java.util.Timer;

/**
 * 正常状态
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class NormalState implements State {
    private CurrentState currentState;

    public NormalState(CurrentState currentState) {
        this.currentState = currentState;
    }

    @Override
    public void eatApple() {
        System.out.println("eatApple: 当前状态--NormalState[正常状态]:增加1积分");
        this.currentState.setIntegral(this.currentState.getIntegral() + 1);
    }

    @Override
    public void eatPoisonousApple() {
        System.out.println("eatPoisonousApple: 当前状态--NormalState[正常状态]:减少1积分");
        this.currentState.setIntegral(this.currentState.getIntegral() - 1);
        if (this.currentState.getIntegral() < 0) {
            this.currentState.setState(this.currentState.getDeathState());
        }
    }

    @Override
    public void eatInvincibleApple() {
        System.out.println("eatInvincibleApple: 当前状态--NormalState[正常状态]:变成无敌状态3秒");
        this.currentState.setState(currentState.getInvincibleState());
        Timer timer = new Timer();
        Task task = new Task(this.currentState, timer);
        timer.schedule(task, 3000);
    }

    @Override
    public void TouchAnObstacle() {
        System.out.println("TouchAnObstacle: 当前状态--NormalState[正常状态]:变成死亡状态");
        this.currentState.setState(currentState.getDeathState());
    }
}
