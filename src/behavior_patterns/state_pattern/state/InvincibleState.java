package behavior_patterns.state_pattern.state;

import behavior_patterns.state_pattern.CurrentState;
import behavior_patterns.state_pattern.Task;

import java.util.Timer;

/**
 * 无敌状态
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class InvincibleState implements State {
    private CurrentState currentState;

    public InvincibleState(CurrentState currentState) {
        this.currentState = currentState;
    }

    @Override
    public void eatApple() {
        System.out.println("eatApple: 当前状态--InvincibleState[无敌状态]:增加1积分");
        this.currentState.setIntegral(this.currentState.getIntegral() + 1);
    }

    @Override
    public void eatPoisonousApple() {
        System.out.println("eatPoisonousApple: 当前状态--InvincibleState[无敌状态]:无视减积分");
    }

    @Override
    public void eatInvincibleApple() {
        System.out.println("eatInvincibleApple: 当前状态--InvincibleState[无敌状态]:刷新无敌时间至3秒");
        Timer timer = new Timer();
        Task task = new Task(this.currentState, timer);
        timer.schedule(task, 3000);
    }

    @Override
    public void TouchAnObstacle() {
        System.out.println("TouchAnObstacle: 当前状态--InvincibleState[无敌状态]:无视障碍物");
    }
}
