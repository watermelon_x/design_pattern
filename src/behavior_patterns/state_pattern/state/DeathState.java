package behavior_patterns.state_pattern.state;

import behavior_patterns.state_pattern.CurrentState;

/**
 * 死亡状态
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class DeathState implements State {

    private CurrentState currentState;

    public DeathState(CurrentState currentState) {
        this.currentState = currentState;
    }

    @Override
    public void eatApple() {
        System.out.println("eatApple: 当前状态--DeathState[死亡状态]:你已经死了,不能进行任何操作");
    }

    @Override
    public void eatPoisonousApple() {
        System.out.println("eatPoisonousApple: 当前状态--DeathState[死亡状态]:你已经死了,不能进行任何操作");
    }

    @Override
    public void eatInvincibleApple() {
        System.out.println("eatInvincibleApple: 当前状态--DeathState[死亡状态]:你已经死了,不能进行任何操作");
    }

    @Override
    public void TouchAnObstacle() {
        System.out.println("TouchAnObstacle: 当前状态--DeathState[死亡状态]:你已经死了,不能进行任何操作");
    }
}
