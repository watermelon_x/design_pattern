package behavior_patterns.state_pattern;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 无敌时间过期
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class Task extends TimerTask {
    private CurrentState currentState;
    private Timer timer;

    public Task(CurrentState currentState, Timer timer) {
        this.currentState = currentState;
        this.timer = timer;
    }

    @Override
    public void run() {
        this.currentState.setState(this.currentState.getNormalState());
        timer.cancel();
    }
}
