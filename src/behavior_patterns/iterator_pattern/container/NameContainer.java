package behavior_patterns.iterator_pattern.container;

import behavior_patterns.iterator_pattern.Iterator;
import behavior_patterns.iterator_pattern.container.Container;

/**
 * 姓名集合
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public class NameContainer implements Container {
    public String names[] = {"A", "B", "C", "D"};

    @Override
    public Iterator getIterator() {
        return new NameIterator();
    }

    private class NameIterator implements Iterator {
        int index;

        @Override
        public boolean hasNext() {
            if (index < names.length) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return names[index++];
            }
            return null;
        }
    }
}
