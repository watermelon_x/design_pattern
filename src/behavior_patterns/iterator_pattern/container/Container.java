package behavior_patterns.iterator_pattern.container;

import behavior_patterns.iterator_pattern.Iterator;

/**
 * 集合
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public interface Container {
    //获取一个迭代器
    Iterator getIterator();
}
