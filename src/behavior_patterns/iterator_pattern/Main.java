package behavior_patterns.iterator_pattern;

import behavior_patterns.iterator_pattern.container.NameContainer;

/**
 * 迭代器模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public class Main {
    public static void main(String[] args) {
        NameContainer container = new NameContainer();
        Iterator iterator = container.getIterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
