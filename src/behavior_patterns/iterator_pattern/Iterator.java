package behavior_patterns.iterator_pattern;

/**
 * 迭代器接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public interface Iterator {
    //判断下一个是否存在
    boolean hasNext();

    //返回下一个元素
    Object next();

}
