package behavior_patterns.command_pattern.command;

/**
 * 命令1
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public class ICommandOne implements ICommand {

    private Receiver receiver = null;

    public ICommandOne(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        this.receiver.doOne();
    }
}
