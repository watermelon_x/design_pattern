package behavior_patterns.command_pattern.command;

/**
 * 命令2
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public class ICommandTwo implements ICommand {

    private Receiver receiver = null;

    public ICommandTwo(Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        this.receiver.doTwo();
    }
}
