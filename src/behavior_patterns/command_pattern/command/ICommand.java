package behavior_patterns.command_pattern.command;

/**
 * 命令接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public interface ICommand {
    void execute();
}

