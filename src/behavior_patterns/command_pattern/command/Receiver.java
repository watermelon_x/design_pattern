package behavior_patterns.command_pattern.command;

/**
 * 所有命令的执行
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public class Receiver {

    public void doOne() {
        System.out.println("执行命令1");
    }

    public void doTwo() {
        System.out.println("执行命令2");

    }
}
