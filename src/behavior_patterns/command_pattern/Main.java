package behavior_patterns.command_pattern;

import behavior_patterns.command_pattern.command.ICommandOne;
import behavior_patterns.command_pattern.command.ICommandTwo;
import behavior_patterns.command_pattern.command.Receiver;

/**
 * 命令模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public class Main {
    public static void main(String[] args) {

        Receiver receiver = new Receiver();

        Invoker invoker = new Invoker();

        invoker.setCommand(new ICommandOne(receiver));
        invoker.runCommand();
        System.out.println("\n");
        invoker.setCommand(new ICommandTwo(receiver));
        invoker.runCommand();
    }
}
