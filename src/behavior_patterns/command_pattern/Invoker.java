package behavior_patterns.command_pattern;

import behavior_patterns.command_pattern.command.ICommand;

import java.util.ArrayList;
import java.util.List;

/**
 * 调用执行命令
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/6/17
 */
public class Invoker {
    List<ICommand> iCommands = new ArrayList<>();

    //设置命令
    public void setCommand(ICommand command) {
        iCommands.add(command);
    }

    //执行命令
    public void runCommand() {
        iCommands.forEach(i -> i.execute());
    }
}
