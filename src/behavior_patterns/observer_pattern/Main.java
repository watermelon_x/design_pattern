package behavior_patterns.observer_pattern;

import behavior_patterns.observer_pattern.observer.MouseListening;
import behavior_patterns.observer_pattern.subject.Mouse;
import behavior_patterns.observer_pattern.subject.Subject;

/**
 * 观察者模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class Main {
    public static void main(String[] args) {

        MouseListening mouseListening = new MouseListening();
        Subject subject = new Mouse(mouseListening);
        subject.subjectUpdate();

        System.out.println("\n");
        System.out.println("----主题注销观察者---");
        subject.logOutObserver(mouseListening);
        subject.subjectUpdate();

        System.out.println("\n");
        System.out.println("----初始化监听时加上主题----");
        MouseListening mouseListening2 = new MouseListening((Mouse) subject);
        subject.registerObserver(mouseListening2);
        subject.subjectUpdate();

        System.out.println("\n");
        System.out.println("----监听者主动注销观察者---");
        mouseListening2.logOutObserver();
        subject.subjectUpdate();
    }
}
