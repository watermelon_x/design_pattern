package behavior_patterns.observer_pattern.subject;

import behavior_patterns.observer_pattern.observer.Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 被监听对象接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public abstract class Subject {
    private List<Observer> observerList = new ArrayList<>();

    public Subject() {

    }

    public Subject(Observer observer) {
        observerList.add(observer);
    }

    /**
     * 删除某个观察者
     *
     * @param observer
     */
    public void logOutObserver(Observer observer) {
        int i = observerList.indexOf(observer);
        if (i != -1) {
            observerList.remove(i);
        }
    }


    /**
     * 注册为观察者
     *
     * @param observer
     */
    public void registerObserver(Observer observer) {
        observerList.add(observer);
    }

    /**
     * 通知所有观察者
     */
    public void notifyObservers() {
        observerList.forEach(o -> o.update());
    }

    /**
     * 当前对象改变方法
     */
    public final void subjectUpdate() {
        run();
        //当前对象改变之后，通知所有观察者,
        // 也可以在此传值给监听者（推）
        // 或者提供getter方法给监听者，监听者自己获取值（拉）
        notifyObservers();
    }

    /**
     * 当前对象做的一些事
     */
    public abstract void run();
}
