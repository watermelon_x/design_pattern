package behavior_patterns.observer_pattern.subject;

import behavior_patterns.observer_pattern.observer.Observer;

/**
 * 鼠标主题
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class Mouse extends Subject {

    public Mouse() {
    }

    public Mouse(Observer observer) {
        super(observer);
    }

    @Override
    public void run() {
        System.out.println("Mouse:我是鼠标,我被点击了一下");
    }
}
