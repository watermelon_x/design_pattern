package behavior_patterns.observer_pattern.observer;

import behavior_patterns.observer_pattern.subject.Mouse;

/**
 * 鼠标监听
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class MouseListening implements Observer {
    //可以用于主动取消监听
    private Mouse mouse;

    public MouseListening() {

    }

    public MouseListening(Mouse mouse) {
        this.mouse = mouse;
    }

    @Override
    public void update() {
        System.out.println("MouseListening: 收到---鼠标被点击了一下");
    }

    /**
     * 主动取消监听
     */
    public void logOutObserver() {
        if (this.mouse != null) {
            this.mouse.logOutObserver(this);
        }
    }

}
