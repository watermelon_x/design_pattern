package behavior_patterns.observer_pattern.observer;

/**
 * 监听者（观察者）
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public interface Observer {

    /**
     * 改变
     */
    void update();
}
