package behavior_patterns.chain_of_responsibility_pattern;

/**
 * 请求类
 *
 * @author watermelon
 * @date 2019/6/4
 */
public class Request {
    private String requestStr;

    public String getRequestStr() {
        return requestStr;
    }

    public void setRequestStr(String requestStr) {
        this.requestStr = requestStr;
    }
}
