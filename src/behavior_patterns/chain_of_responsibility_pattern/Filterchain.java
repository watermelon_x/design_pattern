package behavior_patterns.chain_of_responsibility_pattern;

import behavior_patterns.chain_of_responsibility_pattern.filter.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * 拦截器链（责任链）
 *
 * @author watermelon
 * @date 2019/6/4
 */
public class Filterchain implements Filter {

    int index = 0;
    List<Filter> chain = new ArrayList<>();

    //增加责任链中的子节点
    public Filterchain addFilter(Filter filter) {
        chain.add(filter);
        return this;
    }

    @Override
    public void doFilter(Request request, Filterchain filterchain) {
        //当执行完责任链后返回
        if (index == chain.size()) {
            return;
        }
        //每执行一次，index自增
        Filter filter = chain.get(index);
        index++;
        filter.doFilter(request, filterchain);
    }
}
