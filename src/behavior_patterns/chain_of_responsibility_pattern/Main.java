package behavior_patterns.chain_of_responsibility_pattern;

import behavior_patterns.chain_of_responsibility_pattern.filter.ExpressionFilter;
import behavior_patterns.chain_of_responsibility_pattern.filter.Filter;
import behavior_patterns.chain_of_responsibility_pattern.filter.StringFilter;

/**
 * 责任链模式
 *
 * @author watermelon
 * @date 2019/6/4
 */
public class Main {
    public static void main(String[] args) {
        String requestStr = "黄色,毒气,^:^->这个表情不错";
        Request request = new Request();
        request.setRequestStr(requestStr);
        System.out.println("过滤前---" + request.getRequestStr());
        System.out.println("\n");

        Filter stringFilter = new StringFilter();
        Filter expressionFilter = new ExpressionFilter();
        Filterchain filterchain = new Filterchain();

        filterchain.addFilter(stringFilter).addFilter(expressionFilter);

        filterchain.doFilter(request, filterchain);

        System.out.println("\n");
        System.out.println("过滤后---" + request.getRequestStr());
    }
}
