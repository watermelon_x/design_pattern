package behavior_patterns.chain_of_responsibility_pattern.filter;

import behavior_patterns.chain_of_responsibility_pattern.Filterchain;
import behavior_patterns.chain_of_responsibility_pattern.Request;

/**
 * 拦截器接口
 *
 * @author watermelon
 * @date 2019/6/4
 */
public interface Filter {
    void doFilter(Request request, Filterchain filterchain);
}
