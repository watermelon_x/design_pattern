package behavior_patterns.chain_of_responsibility_pattern.filter;

import behavior_patterns.chain_of_responsibility_pattern.Filterchain;
import behavior_patterns.chain_of_responsibility_pattern.Request;

/**
 * 表情过滤
 *
 * @author watermelon
 * @date 2019/6/4
 */
public class ExpressionFilter implements Filter {
    @Override
    public void doFilter(Request request, Filterchain filterchain) {
        String requestStr = request.getRequestStr();
        String replace = requestStr.replace("^:^", "笑");
        System.out.println("--ExpressionFilter过滤--");
        System.out.println("当前requestStr = " + replace);

        request.setRequestStr(replace);
        filterchain.doFilter(request, filterchain);
    }
}
