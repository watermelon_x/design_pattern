package behavior_patterns.strategy_pattern.role2;

import behavior_patterns.strategy_pattern.behavior.jump.Jump;
import behavior_patterns.strategy_pattern.behavior.play.Play;
import behavior_patterns.strategy_pattern.behavior.sing.Sing;

/**
 * 角色接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public abstract class Role2 {


    /**
     * 唱
     */
    Sing sing;

    /**
     * 玩
     */
    Play play;
    /**
     * 跳
     */
    Jump jump;


    /**
     * 姓名
     */
    public abstract void name();

    /**
     * 唱
     */
    public void sing() {
        this.sing.sing();
    }

    /**
     * 跳
     */
    public void jump() {
        this.jump.jump();
    }

    /**
     * 玩
     */
    public void play() {
        this.play.play();
    }

    public final void run() {
        name();
        sing();
        jump();
        play();
    }


    public void setSing(Sing sing) {
        this.sing = sing;
    }

    public void setJump(Jump jump) {
        this.jump = jump;
    }

    public void setPlay(Play play) {
        this.play = play;
    }
}
