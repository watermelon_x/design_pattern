package behavior_patterns.strategy_pattern.role2;

import behavior_patterns.strategy_pattern.behavior.jump.NotJump;
import behavior_patterns.strategy_pattern.behavior.play.PingPongBall;
import behavior_patterns.strategy_pattern.behavior.sing.LZLH;

/**
 * 西瓜
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class WM2 extends Role2 {

    public WM2() {
        this.sing = new LZLH();
        this.play = new PingPongBall();
        this.jump = new NotJump();
    }

    @Override
    public  void name() {
        System.out.println("我是西瓜,我");
    }
}
