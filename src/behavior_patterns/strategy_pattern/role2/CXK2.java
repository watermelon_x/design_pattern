package behavior_patterns.strategy_pattern.role2;

import behavior_patterns.strategy_pattern.behavior.play.Basketball;
import behavior_patterns.strategy_pattern.behavior.sing.JNTM;

/**
 * cxk
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class CXK2 extends Role2 {

    public CXK2() {
        this.sing = new JNTM();
        this.jump = new behavior_patterns.strategy_pattern.behavior.jump.JNTM();
        this.play = new Basketball();
    }

    @Override
    public  void name() {
        System.out.println("我是cxk2,我");
    }


}
