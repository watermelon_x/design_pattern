package behavior_patterns.strategy_pattern.role;

/**
 * 西瓜
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class WM extends Role {
    @Override
    public  void name() {
        System.out.println("我是西瓜,我会");
    }

    @Override
    public  void jump() {
        System.out.println("不会跳舞");
    }

    public  void sing() {
        System.out.println("唱两只老虎");
    }


    public  void play() {
        System.out.println("打乒乓球");
    }

}
