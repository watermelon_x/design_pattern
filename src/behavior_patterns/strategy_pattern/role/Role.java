package behavior_patterns.strategy_pattern.role;

/**
 * 抽象类
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public abstract class Role {
    /**
     * 姓名
     */
    public abstract void name();

    /**
     * 唱
     */
    public abstract void sing();

    /**
     * 跳
     */
    public abstract void jump();

    /**
     * 玩什么球
     */
    public abstract void play();

    public void run() {
        name();
        sing();
        jump();
        play();
    }


}
