package behavior_patterns.strategy_pattern.role;

/**
 * cxk
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class CXK extends Role {

    @Override
    public void name() {
        System.out.println("我是cxk,我会");
    }

    public  void sing() {
        System.out.println("唱鸡你太美");
    }

    public  void jump() {
        System.out.println("跳鸡你太美");
    }

    public  void play() {
        System.out.println("打篮球");

    }
}
