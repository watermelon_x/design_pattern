package behavior_patterns.strategy_pattern.behavior.sing;

/**
 * 唱
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public interface Sing {
    void sing();
}
