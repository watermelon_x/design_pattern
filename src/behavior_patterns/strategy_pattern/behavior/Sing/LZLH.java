package behavior_patterns.strategy_pattern.behavior.sing;

/**
 * 两只老虎
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class LZLH implements Sing {
    @Override
    public void sing() {
        System.out.println("会唱两只老虎");
    }
}
