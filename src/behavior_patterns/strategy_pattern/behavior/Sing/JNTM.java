package behavior_patterns.strategy_pattern.behavior.sing;

/**
 * 鸡你太美
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class JNTM implements Sing {
    @Override
    public void sing() {
        System.out.println("会唱鸡你太美");
    }
}
