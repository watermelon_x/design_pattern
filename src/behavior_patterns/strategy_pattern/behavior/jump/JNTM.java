package behavior_patterns.strategy_pattern.behavior.jump;

/**
 * 跳鸡你太美
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class JNTM implements Jump {
    @Override
    public void jump() {
        System.out.println("会跳鸡你太美");
    }
}
