package behavior_patterns.strategy_pattern.behavior.jump;

/**
 * 跳
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public interface Jump {
    void jump();
}
