package behavior_patterns.strategy_pattern.behavior.jump;

/**
 * 不会跳
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class NotJump implements Jump {

    @Override
    public void jump() {
        System.out.println("不会跳舞");
    }
}
