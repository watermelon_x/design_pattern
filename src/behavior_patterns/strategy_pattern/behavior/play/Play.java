package behavior_patterns.strategy_pattern.behavior.play;

/**
 * 玩
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public interface Play {
    void play();
}
