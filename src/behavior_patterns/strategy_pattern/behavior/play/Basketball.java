package behavior_patterns.strategy_pattern.behavior.play;

/**
 * 打篮球
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class Basketball implements Play {
    @Override
    public void play() {
        System.out.println("会打篮球");
    }
}
