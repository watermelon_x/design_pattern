package behavior_patterns.strategy_pattern.behavior.play;

/**
 * 乒乓球
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class PingPongBall implements Play {
    @Override
    public void play() {
        System.out.println("会打乒乓球");
    }
}
