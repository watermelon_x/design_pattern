package behavior_patterns.strategy_pattern;

import behavior_patterns.strategy_pattern.behavior.play.Basketball;
import behavior_patterns.strategy_pattern.role.CXK;
import behavior_patterns.strategy_pattern.role.WM;
import behavior_patterns.strategy_pattern.role2.CXK2;
import behavior_patterns.strategy_pattern.role2.WM2;

/**
 * 策略模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class Main {

    public static void main(String[] args) {
        CXK cxk = new CXK();
        cxk.run();

        System.out.println("\n");
        CXK2 cxk2 = new CXK2();
        cxk2.run();

        System.out.println("\n");
        WM wm = new WM();
        wm.run();

        System.out.println("\n");
        WM2 wm2 = new WM2();
        wm2.run();

        System.out.println("\n");
        Basketball basketball = new Basketball();
        wm2.setPlay(basketball);
        wm2.run();

    }
}
