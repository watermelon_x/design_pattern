package behavior_patterns.visitor_pattern;

import behavior_patterns.visitor_pattern.student.XueBa;
import behavior_patterns.visitor_pattern.student.XueZha;
import behavior_patterns.visitor_pattern.visitor.Holiday;
import behavior_patterns.visitor_pattern.visitor.ScoreCheckVisitor;
import behavior_patterns.visitor_pattern.visitor.Visitor;

/**
 * 访问者模式（假设只有学霸和学渣两种角色）
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class Main {
    public static void main(String[] args) {
        XueBa xueba = new XueBa();
        XueZha xueZha = new XueZha();
        Visitor score = new ScoreCheckVisitor();
        score.visitXueBa(xueba);
        score.visitXueZha(xueZha);

        System.out.println("\n");
        Visitor holiday = new Holiday();
        holiday.visitXueBa(xueba);
        holiday.visitXueZha(xueZha);

    }
}
