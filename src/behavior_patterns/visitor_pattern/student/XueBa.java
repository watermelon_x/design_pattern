package behavior_patterns.visitor_pattern.student;

import behavior_patterns.visitor_pattern.visitor.Visitor;

/**
 * 学霸
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class XueBa extends Student {

    @Override
    void accept(Visitor visitor) {
        visitor.visitXueBa(this);
    }
}
