package behavior_patterns.visitor_pattern.student;

import behavior_patterns.visitor_pattern.visitor.Visitor;

/**
 * 学渣
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class XueZha extends Student {
    @Override
    void accept(Visitor visitor) {
        visitor.visitXueZha(this);
    }
}
