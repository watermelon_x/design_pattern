package behavior_patterns.visitor_pattern.student;

import behavior_patterns.visitor_pattern.visitor.Visitor;

/**
 * 学生接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public abstract class Student {

    // 姓名
    private String name;

    abstract void accept(Visitor visitor);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
