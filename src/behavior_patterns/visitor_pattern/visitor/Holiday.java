package behavior_patterns.visitor_pattern.visitor;

import behavior_patterns.visitor_pattern.student.XueBa;
import behavior_patterns.visitor_pattern.student.XueZha;

/**
 * 放假的方法
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class Holiday implements Visitor {
    @Override
    public void visitXueBa(XueBa xueBa) {
        System.out.println("Holiday : visitXueBa---哎，又放假了，还没學够呢");
    }

    @Override
    public void visitXueZha(XueZha xueZha) {
        System.out.println("Holiday : visitXueZha---放假了，hhhhhhhhhhhhhhhhhhhhh");
    }
}
