package behavior_patterns.visitor_pattern.visitor;

import behavior_patterns.visitor_pattern.student.XueBa;
import behavior_patterns.visitor_pattern.student.XueZha;

/**
 * 访问器接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public interface Visitor {

    void visitXueBa(XueBa xueBa);

    void visitXueZha(XueZha xuezha);
}
