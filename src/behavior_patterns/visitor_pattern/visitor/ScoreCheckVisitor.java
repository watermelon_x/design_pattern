package behavior_patterns.visitor_pattern.visitor;

import behavior_patterns.visitor_pattern.student.XueBa;
import behavior_patterns.visitor_pattern.student.XueZha;

/**
 * 查分数方法
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class ScoreCheckVisitor implements Visitor {

    @Override
    public void visitXueBa(XueBa xueBa) {
        System.out.println("ScoreCheckVisitor : visitXueBa---出成绩了，没有考到满分，学霸表示不开心");
    }

    @Override
    public void visitXueZha(XueZha xueZha) {
        System.out.println("ScoreCheckVisitor : visitXueZha---出成绩了，woc，及格了，学渣表示很兴奋");
    }
}
