package behavior_patterns.template_pattern.template2.achieve;

/**
 * sqlServer
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/5
 */
public class SqlServer implements Achieve {
    @Override
    public void specificAchieve() {
        System.out.println("Sql Server数据库连接成功");
    }
}
