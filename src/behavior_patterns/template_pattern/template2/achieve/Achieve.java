package behavior_patterns.template_pattern.template2.achieve;

/**
 * 具体实现接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/5
 */
public interface Achieve {
    /**
     * 具体实现
     */
    void specificAchieve();
}
