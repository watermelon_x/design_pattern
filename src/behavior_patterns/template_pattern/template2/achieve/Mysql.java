package behavior_patterns.template_pattern.template2.achieve;

/**
 * mysql
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/5
 */
public class Mysql implements Achieve {
    @Override
    public void specificAchieve() {
        System.out.println("mysql数据库连接成功");
    }
}
