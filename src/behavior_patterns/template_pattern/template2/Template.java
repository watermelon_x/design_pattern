package behavior_patterns.template_pattern.template2;

import behavior_patterns.template_pattern.template2.achieve.Achieve;

/**
 * 模板类
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/5
 */
public class Template {

    public void run(Achieve achieve) {
        System.out.println("数据库开始连接----");
        achieve.specificAchieve();
        System.out.println("关闭数据库连接----");
    }
}
