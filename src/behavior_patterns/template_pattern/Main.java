package behavior_patterns.template_pattern;

import behavior_patterns.template_pattern.template.CJ;
import behavior_patterns.template_pattern.template.WZRY;
import behavior_patterns.template_pattern.template2.Template;
import behavior_patterns.template_pattern.template2.achieve.Achieve;
import behavior_patterns.template_pattern.template2.achieve.Mysql;
import behavior_patterns.template_pattern.template2.achieve.SqlServer;

/**
 * 模板模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class Main {
    public static void main(String[] args) {

        System.out.println("------------------template1-------------");
        WZRY wzry = new WZRY();
        wzry.run();


        System.out.println("\n");

        CJ cj = new CJ();
        cj.run();


        System.out.println("\n");
        System.out.println("\n");
        System.out.println("------------------template2-------------");

        Achieve mysql = new Mysql();
        Achieve sqlServer = new SqlServer();
        Template template = new Template();
        template.run(mysql);
        template.run(sqlServer);
        System.out.println("\n");
        template.run(() -> System.out.println("Oracle数据库连接成功"));

    }
}
