package behavior_patterns.template_pattern.template;

import java.time.LocalDateTime;

/**
 * 模板方法
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public abstract class Template {

    /**
     * 游戏初始化
     */
    public abstract void gameInit();

    /**
     * 游戏中
     */
    public abstract void inGame();

    /**
     * 游戏结束
     */
    public abstract void gameOver();

    /**
     * 模板方法主体
     */
    public final void run() {
        System.out.println("游戏开始--当前时间:" + LocalDateTime.now());
        gameInit();
        inGame();
        gameOver();
        System.out.println("游戏结束--当前时间:" + LocalDateTime.now());
    }
}
