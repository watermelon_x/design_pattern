package behavior_patterns.template_pattern.template;

/**
 * 王者荣耀
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class WZRY extends Template {
    @Override
    public void gameInit() {
        System.out.println("王者荣耀游戏开始---");
    }

    @Override
    public void inGame() {
        System.out.println("王者荣耀游戏中---");
    }

    @Override
    public void gameOver() {
        System.out.println("王者荣耀游戏结束---");
    }
}
