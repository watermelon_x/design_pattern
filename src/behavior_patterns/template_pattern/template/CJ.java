package behavior_patterns.template_pattern.template;

/**
 * 吃鸡
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class CJ extends Template {
    @Override
    public void gameInit() {
        System.out.println("吃鸡游戏开始---");
    }

    @Override
    public void inGame() {
        System.out.println("吃鸡游戏中---");
    }

    @Override
    public void gameOver() {
        System.out.println("吃鸡游戏结束---");
    }
}
