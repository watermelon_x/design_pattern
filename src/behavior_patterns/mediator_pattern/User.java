package behavior_patterns.mediator_pattern;

/**
 * 聊天的人
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class User {


    public User(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 在聊天室中发言
     *
     * @param msg
     */
    public void sendMsg(String msg) {
        ChatRoom.showMsg(this, msg);
    }

    /**
     * 发送消息给指定人
     *
     * @param user
     * @param msg
     */
    public void sendMsgTo(User user, String msg) {
        ChatRoom.sendMsgTo(this, user, msg);
    }
}
