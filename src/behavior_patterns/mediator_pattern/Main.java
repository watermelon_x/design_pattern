package behavior_patterns.mediator_pattern;

/**
 * 中介者模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class Main {
    public static void main(String[] args) {
        User A = new User("A");
        User B = new User("B");

        ChatRoom.addUser(A);
        ChatRoom.addUser(B);

        System.out.println("\n");
        A.sendMsg("大家好,我是A");

        System.out.println("\n");
        B.sendMsg("大家好,我是B");

        System.out.println("\n");
        A.sendMsgTo(B, "请问你是B吗？");


    }
}
