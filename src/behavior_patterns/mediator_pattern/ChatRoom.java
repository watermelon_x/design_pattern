package behavior_patterns.mediator_pattern;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 聊天室（中介者）
 * 统一处理消息的转发
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class ChatRoom {
    private static List<User> users = new ArrayList<>();

    /**
     * 在聊天室中显示消息
     */
    public static void showMsg(User user, String msg) {
        for (User user1 : users) {
            System.out.println(user1.getName() + ":   收到来自  " + user.getName() + "  的消息:" + msg + "------ " + LocalDateTime.now());
        }
    }

    /**
     * 发送消息给指定人
     *
     * @param fromUser
     * @param toUser
     * @param msg
     */
    public static void sendMsgTo(User fromUser, User toUser, String msg) {
        System.out.println(toUser.getName() + ":   收到来自  " + fromUser.getName() + "  的消息:" + msg + "------ " + LocalDateTime.now());
    }

    public static void addUser(User user) {
        users.add(user);
    }
}
