package behavior_patterns.null_object_pattern;

import behavior_patterns.null_object_pattern.persion.AbstractPersion;
import behavior_patterns.null_object_pattern.persion.NullPersion;
import behavior_patterns.null_object_pattern.persion.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * 造人工厂
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class PersonFactory {
    private static List<String> personList = new ArrayList<String>() {{
        this.add("A");
        this.add("B");
        this.add("C");
    }};


    private PersonFactory() {

    }

    public static AbstractPersion getPerson(String name) {

        if (personList.contains(name)) {
            return new Person(name);
        }
        return new NullPersion();
    }
}
