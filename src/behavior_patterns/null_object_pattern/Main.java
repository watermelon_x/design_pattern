package behavior_patterns.null_object_pattern;

import behavior_patterns.null_object_pattern.persion.AbstractPersion;

/**
 * 空对象模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class Main {
    public static void main(String[] args) {
        AbstractPersion a = PersonFactory.getPerson("A");
        System.out.println(a.getName());
        AbstractPersion d = PersonFactory.getPerson("D");
        System.out.println(d.getName());
    }
}
