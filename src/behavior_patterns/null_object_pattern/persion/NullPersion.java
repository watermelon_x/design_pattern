package behavior_patterns.null_object_pattern.persion;

/**
 * 空的 人
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class NullPersion extends AbstractPersion {


    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public String getName() {
        System.out.println();
        return "这是一个空对象,没有名字";
    }
}
