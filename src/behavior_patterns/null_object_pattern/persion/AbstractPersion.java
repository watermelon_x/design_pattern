package behavior_patterns.null_object_pattern.persion;

/**
 * 人的 接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public abstract class AbstractPersion {


    protected String name;

    public abstract boolean isNull();

    public abstract String getName();

}
