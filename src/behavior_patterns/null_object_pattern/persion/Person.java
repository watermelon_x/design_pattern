package behavior_patterns.null_object_pattern.persion;

/**
 * 人
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/2
 */
public class Person extends AbstractPersion {


    public Person(String name) {
        this.name = name;
    }

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
