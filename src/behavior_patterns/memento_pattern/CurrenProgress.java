package behavior_patterns.memento_pattern;

/**
 * 当前进度
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class CurrenProgress {

    public CurrenProgress(String currenProgressContent) {
        this.currenProgressContent = currenProgressContent;
    }

    /**
     * 当前进度内容
     */
    private String currenProgressContent;

    public String getCurrenProgressContent() {
        return this.currenProgressContent;
    }

    public void setCurrenProgressContent(String currenProgressContent) {
        this.currenProgressContent = currenProgressContent;
    }

    /**
     * 读取存档
     *
     * @param archive
     */
    public void ReadArchived(Archive archive) {
        System.out.println("读取存档中。。。");
        this.currenProgressContent = archive.getArchivedContent();
    }

    /**
     * 返回一个当前进度作为存档内容
     */
    public Archive archive() {
        System.out.println("存档中。。。");
        return new Archive(this.currenProgressContent);
    }

    /**
     * 显示当前进度
     */
    public void run() {
        System.out.println(this.currenProgressContent);
    }
}
