package behavior_patterns.memento_pattern;

/**
 * 存档点
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class Archive {
    public Archive(String currenProgressContent) {
        this.archivedContent = currenProgressContent;
    }

    /**
     * 存档内容
     */
    private String archivedContent;

    public String getArchivedContent() {
        return archivedContent;
    }

    public void setArchivedContent(String archivedContent) {
        this.archivedContent = archivedContent;
    }
}
