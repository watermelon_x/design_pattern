package behavior_patterns.memento_pattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 所有的存档管理
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class ArchivesManagement {
    private List<Archive> archives = new ArrayList<>();

    /**
     * 增加存档
     *
     * @param archive
     */
    public void addArchive(Archive archive) {
        archives.add(archive);
    }

    /**
     * 返回一个存档
     *
     * @param index
     * @return
     */
    public Archive getArchive(int index) {
        return archives.get(index);
    }

    public List<Archive> getArchives() {
        return archives;
    }

    public void setArchives(List<Archive> archives) {
        this.archives = archives;
    }
}
