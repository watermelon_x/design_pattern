package behavior_patterns.memento_pattern;

/**
 * 备忘录模式
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/1
 */
public class Main {
    public static void main(String[] args) {
        CurrenProgress currenProgress = new CurrenProgress("游戏开始");
        currenProgress.run();

        System.out.println("\n");
        currenProgress.setCurrenProgressContent("第一个存档点");
        currenProgress.run();
        Archive archive = currenProgress.archive();
        ArchivesManagement archivesManagement = new ArchivesManagement();
        archivesManagement.addArchive(archive);
        System.out.println("存档成功");

        System.out.println("\n");
        currenProgress.setCurrenProgressContent("第二个存档点");
        currenProgress.run();
        Archive archive2 = currenProgress.archive();
        archivesManagement.addArchive(archive2);
        System.out.println("存档成功");

        System.out.println("\n");
        currenProgress.setCurrenProgressContent("oh,shit,死掉了");
        currenProgress.run();
        System.out.println("准备读取存档,回到上一个存档点");
        Archive archive1 = archivesManagement.getArchive(archivesManagement.getArchives().size() - 1);
        currenProgress.ReadArchived(archive1);
        System.out.println("回到了--");
        currenProgress.run();


        System.out.println("\n");
        System.out.println("直接读取第一个存档点");
        System.out.println("回到了--");
        currenProgress.ReadArchived(archivesManagement.getArchive(0));
        currenProgress.run();
    }
}

