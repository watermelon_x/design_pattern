package creative_patterns.factory_pattern.factory_method;

import creative_patterns.factory_pattern.factory_method.cofe.Coffe;

/**
 * 抽象咖啡类
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public abstract class AbstractCoffe {

    /**
     * 返回一种咖啡，具体实现由子类决定
     *
     * @return
     */
    public abstract Coffe createCoffe(String coffeName);
}
