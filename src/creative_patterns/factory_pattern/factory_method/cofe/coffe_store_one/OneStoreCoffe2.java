package creative_patterns.factory_pattern.factory_method.cofe.coffe_store_one;

import creative_patterns.factory_pattern.factory_method.cofe.Coffe;

/**
 * 第一家店的第一种咖啡
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public class OneStoreCoffe2 implements Coffe {
    @Override
    public void coffeName() {
        System.out.println("OneStoreCoffe2:我是第一家店的第2种咖啡");
    }
}
