package creative_patterns.factory_pattern.factory_method.cofe.coffe_store_one;

import creative_patterns.factory_pattern.factory_method.cofe.Coffe;

/**
 * 第一家店的第一种咖啡
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public class OneStoreCoffe1 implements Coffe {
    @Override
    public void coffeName() {
        System.out.println("OneStoreCoffe1:我是第一家店的第1种咖啡");
    }
}
