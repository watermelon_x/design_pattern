package creative_patterns.factory_pattern.factory_method.cofe;

/**
 * 弹窗接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public interface Coffe {
    void coffeName();
}
