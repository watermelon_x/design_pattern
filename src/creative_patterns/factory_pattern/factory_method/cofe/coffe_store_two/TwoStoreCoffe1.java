package creative_patterns.factory_pattern.factory_method.cofe.coffe_store_two;

import creative_patterns.factory_pattern.factory_method.cofe.Coffe;

/**
 * 第二家店的第一种咖啡
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public class TwoStoreCoffe1 implements Coffe {
    @Override
    public void coffeName() {
        System.out.println("TwoStoreCoffe1:我是第二家店的第1种咖啡");
    }
}
