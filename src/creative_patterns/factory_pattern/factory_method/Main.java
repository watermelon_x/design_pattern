package creative_patterns.factory_pattern.factory_method;

import creative_patterns.factory_pattern.factory_method.cofe.Coffe;
import creative_patterns.factory_pattern.factory_method.store.OneStore;
import creative_patterns.factory_pattern.factory_method.store.TwoStore;

/**
 * 工厂方法
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public class Main {


    public static void main(String[] args) {
        OneStore oneStore = new OneStore();
        Coffe oneStoreCoffe1 = oneStore.createCoffe("coffe1");
        oneStoreCoffe1.coffeName();


        TwoStore twoStore = new TwoStore();
        Coffe twoStoreCoffe1 = twoStore.createCoffe("coffe1");
        twoStoreCoffe1.coffeName();

    }
}
