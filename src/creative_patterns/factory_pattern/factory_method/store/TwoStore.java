package creative_patterns.factory_pattern.factory_method.store;

import creative_patterns.factory_pattern.factory_method.AbstractCoffe;
import creative_patterns.factory_pattern.factory_method.cofe.Coffe;
import creative_patterns.factory_pattern.factory_method.cofe.coffe_store_two.TwoStoreCoffe1;
import creative_patterns.factory_pattern.factory_method.cofe.coffe_store_two.TwoStoreCoffe2;

/**
 * 第二家店
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public class TwoStore extends AbstractCoffe {

    @Override
    public Coffe createCoffe(String coffeName) {
        if ("coffe1".equals(coffeName)) {
            return new TwoStoreCoffe1();
        }
        if ("coffe2".equals(coffeName)) {
            return new TwoStoreCoffe2();
        }
        return null;
    }
}
