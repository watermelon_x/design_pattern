package creative_patterns.factory_pattern.factory_method.store;

import creative_patterns.factory_pattern.factory_method.AbstractCoffe;
import creative_patterns.factory_pattern.factory_method.cofe.Coffe;
import creative_patterns.factory_pattern.factory_method.cofe.coffe_store_one.OneStoreCoffe1;
import creative_patterns.factory_pattern.factory_method.cofe.coffe_store_one.OneStoreCoffe2;

/**
 * 第一家店
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/8
 */
public class OneStore extends AbstractCoffe {

    @Override
    public Coffe createCoffe(String coffeName) {
        if ("coffe1".equals(coffeName)) {
            return new OneStoreCoffe1();
        }
        if ("coffe2".equals(coffeName)) {
            return new OneStoreCoffe2();
        }
        return null;
    }
}
