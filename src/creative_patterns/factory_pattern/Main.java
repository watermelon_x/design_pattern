package creative_patterns.factory_pattern;

/**
 * @author watermelon
 * @date 2019/5/24
 */
public class Main {

    public static void main(String[] args) {
        //弹一个确认弹窗
        FactoryInterface c1 = ClassFactory.getInstance("Class1");
        c1.run();
        //弹一个提示弹窗
        FactoryInterface c2 = ClassFactory.getInstance("Class2");
        c2.run();
        //弹一个带输入框的弹窗
        FactoryInterface c3 = ClassFactory.getInstance("Class3");
        c3.run();

    }
}
