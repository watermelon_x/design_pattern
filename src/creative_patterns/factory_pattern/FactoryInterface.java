package creative_patterns.factory_pattern;

/**
 * 工厂接口
 *
 * @author watermelon
 * @date 2019/5/24
 */
public interface FactoryInterface {
    void run();
}
