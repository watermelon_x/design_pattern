package creative_patterns.factory_pattern;

/**
 * 3
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Class3 implements FactoryInterface {
    @Override
    public void run() {
        System.out.println("弹一个带输入框的弹窗");
    }
}
