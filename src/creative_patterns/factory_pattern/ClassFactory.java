package creative_patterns.factory_pattern;

/**
 * 工厂
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class ClassFactory {

    public static FactoryInterface getInstance(String className) {
        switch (className) {
            case "Class1":
                return new Class1();
            case "Class2":
                return new Class2();
            case "Class3":
                return new Class3();
            default:
                return null;
        }
    }
}
