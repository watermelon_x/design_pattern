package creative_patterns.factory_pattern;


/**
 * 2
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Class2 implements FactoryInterface {
    @Override
    public void run() {
        System.out.println("弹一个提示弹窗");
    }
}
