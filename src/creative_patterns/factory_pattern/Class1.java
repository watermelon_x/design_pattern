package creative_patterns.factory_pattern;

/**
 * 1
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Class1 implements FactoryInterface {
    @Override
    public void run() {
        System.out.println("弹一个确认弹窗");
    }
}
