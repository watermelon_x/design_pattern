package creative_patterns.abstract_factory_pattern;

import creative_patterns.abstract_factory_pattern.color.Blue;
import creative_patterns.abstract_factory_pattern.color.Violet;
import creative_patterns.abstract_factory_pattern.shape.Circle;
import creative_patterns.abstract_factory_pattern.shape.Square;

/**
 * 实例生产者
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class AbstractProducer {

    public static AbstractFactory getInstance(String name) {
        if (name == null) {
            return null;
        }
        switch (name) {
            case "Circle":
                return new Circle();
            case "Square":
                return new Square();
            case "Blue":
                return new Blue();
            case "Violet":
                return new Violet();
            default:
                return null;
        }
    }
}
