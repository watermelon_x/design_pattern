package creative_patterns.abstract_factory_pattern;

/**
 * 抽象工厂模式
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Main {

    public static void main(String[] args) {
        AbstractFactory blue = AbstractProducer.getInstance("Blue");
        blue.getColor();

        AbstractFactory violet = AbstractProducer.getInstance("Violet");
        violet.getColor();

        AbstractFactory circular = AbstractProducer.getInstance("Circle");
        circular.getShape();

        AbstractFactory square = AbstractProducer.getInstance("Square");
        square.getShape();

    }
}
