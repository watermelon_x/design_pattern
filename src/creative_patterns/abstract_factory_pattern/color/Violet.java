package creative_patterns.abstract_factory_pattern.color;

/**
 * 紫色
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Violet implements Color {
    @Override
    public void getColor() {
        System.out.println("violet...run");
    }

    @Override
    public void getShape() {

    }


}
