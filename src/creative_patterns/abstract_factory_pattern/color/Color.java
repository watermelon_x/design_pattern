package creative_patterns.abstract_factory_pattern.color;

import creative_patterns.abstract_factory_pattern.AbstractFactory;

/**
 * 颜色接口
 *
 * @author watermelon
 * @date 2019/5/24
 */
public interface Color extends AbstractFactory {

}
