package creative_patterns.abstract_factory_pattern.color;

/**
 * 颜色工厂
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class ColorFactory {

    public static Color getInstance(String colorName) {
        if (colorName == null) {
            return null;
        }
        switch (colorName) {
            case "Blue":
                return new Blue();
            case "Violet":
                return new Violet();
            default:
                return null;
        }
    }
}
