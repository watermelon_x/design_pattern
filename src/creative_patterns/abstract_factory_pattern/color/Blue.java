package creative_patterns.abstract_factory_pattern.color;

/**
 * 蓝色
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Blue implements Color {



    @Override
    public void getColor() {
        System.out.println("blue...run");
    }

    @Override
    public void getShape() {

    }
}
