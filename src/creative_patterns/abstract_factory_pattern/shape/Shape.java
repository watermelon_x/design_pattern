package creative_patterns.abstract_factory_pattern.shape;

import creative_patterns.abstract_factory_pattern.AbstractFactory;

/**
 * 形状接口
 *
 * @author watermelon
 * @date 2019/5/24
 */
public interface Shape extends AbstractFactory {

}
