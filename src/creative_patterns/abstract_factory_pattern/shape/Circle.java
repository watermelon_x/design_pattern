package creative_patterns.abstract_factory_pattern.shape;

/**
 * 圆
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Circle implements Shape {
    @Override
    public void getColor() {

    }

    @Override
    public void getShape() {
        System.out.println("circular...run");
    }

}
