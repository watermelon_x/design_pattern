package creative_patterns.abstract_factory_pattern.shape;

/**
 * 图形工厂
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class ShapeFactory {

    public static Shape getInstance(String shapeName) {
        if (shapeName == null) {
            return null;
        }
        switch (shapeName) {
            case "Circle":
                return new Circle();
            case "Square":
                return new Square();
            default:
                return null;
        }
    }
}
