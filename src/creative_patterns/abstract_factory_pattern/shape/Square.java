package creative_patterns.abstract_factory_pattern.shape;

/**
 * 正方形
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Square implements Shape {
    @Override
    public void getColor() {

    }

    @Override
    public void getShape() {
        System.out.println("square...run");
    }

}
