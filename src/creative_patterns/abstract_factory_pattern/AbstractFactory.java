package creative_patterns.abstract_factory_pattern;

/**
 * 抽象工厂
 *
 * @author watermelon
 * @date 2019/5/24
 */
public interface AbstractFactory {
    void getColor();
    void getShape();
}
