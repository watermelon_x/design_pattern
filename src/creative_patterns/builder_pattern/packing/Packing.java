package creative_patterns.builder_pattern.packing;

/**
 * 包装接口
 *
 * @author watermelon
 * @date 2019/5/27
 */
public interface Packing {
    String pack();
}
