package creative_patterns.builder_pattern.packing;

/**
 * 纸包装
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Wrapper implements Packing {
    @Override
    public String pack() {
        return "Wrapper";
    }
}
