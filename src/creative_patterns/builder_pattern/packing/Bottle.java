package creative_patterns.builder_pattern.packing;

/**
 * 瓶包装
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Bottle implements Packing {
    @Override
    public String pack() {
        return "Bottle";
    }
}
