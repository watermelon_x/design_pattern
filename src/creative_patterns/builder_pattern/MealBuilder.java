package creative_patterns.builder_pattern;

import creative_patterns.builder_pattern.item.burger.ChickenBurger;
import creative_patterns.builder_pattern.item.burger.VegBurger;
import creative_patterns.builder_pattern.item.cold_drink.Coke;
import creative_patterns.builder_pattern.item.cold_drink.Tea;

/**
 * 构建一顿饭
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class MealBuilder {

    public Meal prepareVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new VegBurger());
        meal.addItem(new Coke());
        return meal;
    }

    public Meal prepareNonVegMeal() {
        Meal meal = new Meal();
        meal.addItem(new ChickenBurger());
        meal.addItem(new Tea());
        return meal;
    }
}
