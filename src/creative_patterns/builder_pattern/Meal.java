package creative_patterns.builder_pattern;

import creative_patterns.builder_pattern.item.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 一顿饭
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Meal {
    private List<Item> items = new ArrayList<>();

    public void addItem(Item item) {
        items.add(item);
    }

    public float getTotalPrice() {
        float totalPrice = 0.0f;
        for (Item item : items) {
            totalPrice += item.price();
        }
        return totalPrice;
    }

    public String getAllName() {
        return items.stream().map(i -> i.name()).collect(Collectors.joining(""));
    }

    public void showItems() {
        for (Item item : items) {
            System.out.print("Item : " + item.name());
            System.out.print(", Packing : " + item.packing().pack());
            System.out.println(", Price : " + item.price());
        }
    }
}
