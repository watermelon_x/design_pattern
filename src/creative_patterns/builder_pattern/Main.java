package creative_patterns.builder_pattern;

/**
 * 建造者模式
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Main {

    public static void main(String[] args) {
        MealBuilder mealBuilder = new MealBuilder();

        Meal meal = mealBuilder.prepareNonVegMeal();
        meal.showItems();
        System.out.println("name:" + meal.getAllName() + "   totalPrice:" + meal.getTotalPrice());

        Meal meal2 = mealBuilder.prepareVegMeal();
        meal2.showItems();
        System.out.println("name:" + meal2.getAllName() + "   totalPrice:" + meal2.getTotalPrice());

    }
}
