package creative_patterns.builder_pattern.item.burger;

/**
 * 鸡肉汉堡
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class ChickenBurger extends Burger {
    @Override
    public String name() {
        return "ChickenBurger";
    }

    @Override
    public float price() {
        return 30.0f;
    }
}
