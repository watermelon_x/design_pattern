package creative_patterns.builder_pattern.item.burger;

/**
 * 蔬菜汉堡
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class VegBurger extends Burger {
    @Override
    public String name() {
        return "VegBurger";
    }

    @Override
    public float price() {
        return 20.5f;
    }
}
