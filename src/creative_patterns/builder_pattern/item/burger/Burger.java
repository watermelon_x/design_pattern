package creative_patterns.builder_pattern.item.burger;


import creative_patterns.builder_pattern.item.Item;
import creative_patterns.builder_pattern.packing.Packing;
import creative_patterns.builder_pattern.packing.Wrapper;

/**
 * 汉堡
 *
 * @author watermelon
 * @date 2019/5/27
 */
public abstract class Burger implements Item {

    @Override
    public Packing packing() {
        return new Wrapper();
    }
}
