package creative_patterns.builder_pattern.item.cold_drink;

import creative_patterns.builder_pattern.item.Item;
import creative_patterns.builder_pattern.packing.Bottle;
import creative_patterns.builder_pattern.packing.Packing;

/**
 * 冷饮
 *
 * @author watermelon
 * @date 2019/5/27
 */
public abstract class ColdDrink implements Item {


    @Override
    public Packing packing() {
        return new Bottle();
    }


}
