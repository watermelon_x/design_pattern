package creative_patterns.builder_pattern.item.cold_drink;

/**
 * 可乐
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Coke extends ColdDrink {
    @Override
    public String name() {
        return "Coke";
    }

    @Override
    public float price() {
        return 4.0f;
    }
}
