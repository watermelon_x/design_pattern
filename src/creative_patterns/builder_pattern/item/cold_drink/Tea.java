package creative_patterns.builder_pattern.item.cold_drink;

/**
 * 茶
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Tea extends ColdDrink {
    @Override
    public String name() {
        return "Tea";
    }

    @Override
    public float price() {
        return 5.5f;
    }
}
