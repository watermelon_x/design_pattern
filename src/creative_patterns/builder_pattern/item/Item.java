package creative_patterns.builder_pattern.item;

import creative_patterns.builder_pattern.packing.Packing;

/**
 * 食物的条目和包装
 *
 * @author watermelon
 * @date 2019/5/27
 */
public interface Item {
    String name();

    Packing packing();

    float price();
}
