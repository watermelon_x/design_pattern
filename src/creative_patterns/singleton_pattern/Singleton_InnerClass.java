package creative_patterns.singleton_pattern;

/**
 * 登记式
 * 延迟初始化
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Singleton_InnerClass {
    private static class Singleton_InnerClass_Holder {
        private static final Singleton_InnerClass SINGLETON_INNERCLASS = new Singleton_InnerClass();
    }

    private Singleton_InnerClass() {
    }

    public static Singleton_InnerClass getInstance() {
        return Singleton_InnerClass_Holder.SINGLETON_INNERCLASS;
    }
}
