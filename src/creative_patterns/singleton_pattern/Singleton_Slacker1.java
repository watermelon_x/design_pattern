package creative_patterns.singleton_pattern;

/**
 * 懒汉式单例模式
 * 线程不安全
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Singleton_Slacker1 {

    private static Singleton_Slacker1 singleton_slacker;

    private Singleton_Slacker1() {

    }

    public static Singleton_Slacker1 getInstance() {
        if (singleton_slacker == null) {
            singleton_slacker = new Singleton_Slacker1();
        }
        return singleton_slacker;
    }
}
