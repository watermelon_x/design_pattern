package creative_patterns.singleton_pattern;

/**
 * 饿汉式单例模式
 * 使用最多，线程安全，效率高
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Singleton_HungryMan {

    private static Singleton_HungryMan singleton_hungryMan = new Singleton_HungryMan();

    private Singleton_HungryMan() {

    }

    public static Singleton_HungryMan getInstance() {
        return singleton_hungryMan;
    }
}
