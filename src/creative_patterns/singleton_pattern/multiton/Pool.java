package creative_patterns.singleton_pattern.multiton;

import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * 连接池(模板方法+多例  自旋获取连接)
 *
 * @author watermelon
 * @time 2020/4/17
 */
public class Pool {

    public Pool() {
        this(5);
    }

    public Pool(int _size) {
        this.size = _size;
        list = new Vector<>(size);
        for (int i = 0; i < size; i++) {
            list.add(new Connection());
        }
    }

    private int size;
    /**
     * 连接
     */
    private volatile List<Connection> list;

    /**
     * 获取连接超时时间
     */
    private static final long timeout = 4000;


    public void execute(Content content) {
        //从连接池获取连接
        Connection connection = getCon();
        try {
            content.run(connection);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //归还连接
            connection.setState(0);
            System.out.println("归还连接成功！");
        }
    }

    //获取连接
    public Connection getCon() {
        long start = System.currentTimeMillis();
        for (; ; ) {
            for (int i = 0; i < size; i++) {
                Connection c = list.get(i);
                if (c.checkAndSetStates()) {
                    return c;
                }
            }

            System.out.println("连接已使用完，等待其他人归还连接");
            if (System.currentTimeMillis() - start > timeout) {
                throw new RuntimeException("获取连接超时");
            }
            //否则等待，然后再次去获取
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    //丢进pool里待执行的内容
    @FunctionalInterface
    interface Content {
        void run(Connection connection);
    }


    /**
     * 连接类
     */
    class Connection {
        //因为是内部类，所以Pool类是可以调用这个私有构造的
        //私有内部类是作为外部类的一个内部成员存在的，所以可以调用
        private Connection() {

        }

        // 0 free  1 被使用中
        private volatile int state;

        public int select() {
            return new Random().nextInt(100);
        }

        //单独判断
        public boolean isFree() {
            return state == 0;
        }

        public void setState(int _state) {
            this.state = _state;
        }

        //为了保证原子操作，将判断状态和设置状态结合
        public synchronized boolean checkAndSetStates() {
            if (this.state == 0) {
                this.setState(1);
                return true;
            }
            return false;
        }
    }
}


class Main {
    public static void main(String[] args) {
        test1();
//        test2();
    }

    public static void test1() {
        Pool pool = new Pool(1);

        new Thread(() -> {
            pool.execute((connection) -> {
                System.out.println("一号连接上了");
                System.out.println("一号查询结果为  " + connection.select());
                System.out.println("一号占用连接3秒");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print("一号准备归还连接     ");
                throw new RuntimeException("123");
            });
        }).start();

        new Thread(() -> {
            pool.execute((connection) -> {
                System.out.println("二号连接上了");
                System.out.println("二号查询结果为  " + connection.select());
                System.out.println("二占用连接3秒");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print("二号准备归还连接       ");
            });
        }).start();
    }

    public static void test2() {
        Pool pool = new Pool(2);
        for (int i = 0; i < 20; i++) {
            int j = i;
            new Thread(() -> {
                pool.execute((connection) -> {
                    System.out.println(j + "号连接上了");
                    System.out.println(j + "号查询结果为  " + connection.select());
                    System.out.println(j + "号占用连接0.5秒");
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.print(j + "号准备归还连接     ");
                });
            }).start();
        }
    }
}


