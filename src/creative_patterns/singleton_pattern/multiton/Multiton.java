package creative_patterns.singleton_pattern.multiton;

import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * 单例扩展 -》多例模式
 *
 * @author watermelon
 * @time 2020/4/16
 */
public class Multiton {

    private Multiton() {

    }

    private static final int NUMBER = 5;
    private static final List<Multiton> MS = new Vector<>(NUMBER);
    private static Random random = new Random();

    static {
        for (int i = 0; i < NUMBER; i++) {
            MS.add(new Multiton());
        }
    }

    public static Multiton getInstance() {
        int i = random.nextInt(NUMBER);
        System.out.println(i);
        return MS.get(i);
    }


    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            System.out.println(Multiton.getInstance());

        }
    }
}
