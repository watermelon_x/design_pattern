package creative_patterns.singleton_pattern;

/**
 * 单例模式 双锁
 * 直接使用 synchronized修饰getInstance方法会导致每次调用都同步，性能不好
 * 双锁单例先判断是否为空，如果为空才同步，不会太多影响到性能。
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/13
 */
public class Singleton_DoubleCheck {
    //volatile 关键字
    //在new 对象时有4个步骤（最后一步执行完才是完成new一个对象），由于cpu中有指令重排序（将4个步骤的顺序打乱）
    //但是在还没有完成4个步骤时，对象就已经不为空，当其他地方在这个时候使用这个对象，就会报错。
    //volatile 关键字  就是提醒cpu对此对象 关闭 指令重排序
    //在多线程中可能会使用到此关键字（ps:用在多线程的访问上，不用在多线程中的修改上！！！）
    private static volatile Singleton_DoubleCheck singleton_doubleCheck;

    private Singleton_DoubleCheck() {

    }

    public static Singleton_DoubleCheck getInstance() {
        //用于在第一次使用时 让方法同步
        if (singleton_doubleCheck == null) {
            synchronized (Singleton_DoubleCheck.class) {
                //单例
                if (singleton_doubleCheck == null) {
                    singleton_doubleCheck = new Singleton_DoubleCheck();
                }
            }
        }
        return singleton_doubleCheck;
    }

}