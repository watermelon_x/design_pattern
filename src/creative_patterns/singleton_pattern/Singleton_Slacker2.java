package creative_patterns.singleton_pattern;

/**
 * 懒汉式单例模式
 * 线程安全,效率很低
 *
 * @author watermelon
 * @date 2019/5/24
 */
public class Singleton_Slacker2 {

    private static volatile Singleton_Slacker2 singleton_slacker;

    private Singleton_Slacker2() {

    }

    public static synchronized Singleton_Slacker2 getInstance() {
        if (singleton_slacker == null) {
            singleton_slacker = new Singleton_Slacker2();
        }
        return singleton_slacker;
    }
}
