package creative_patterns.prototype_pattern.copy_test;

/**
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class Person implements Cloneable {

    private String name;

    private Age age;


    /**
     * 浅复制
     *
     * @return
     * @throws CloneNotSupportedException
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * 深复制
     *
     * @return
     * @throws CloneNotSupportedException
     */
    protected Object createClone() throws CloneNotSupportedException {
        Person clonePerson = (Person) super.clone();
        Age cloneAge = (Age) clonePerson.getAge().clone();
        clonePerson.setAge(cloneAge);
        return clonePerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Age getAge() {
        return age;
    }

    public void setAge(Age age) {
        this.age = age;
    }
}
