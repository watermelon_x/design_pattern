package creative_patterns.prototype_pattern.copy_test;

/**
 * 复制测试
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class TestMain {
    public static void main(String[] args) {
        System.out.println("------------浅复制--------------");
        /**
         * String类型属于引用数据类型，不属于基本数据类型
         * 但是String类型的数据是存放在常量池中的，也就是无法修改的
         * 所以浅复制后name属性不会随被克隆对象改变而改变
         *
         * 浅复制之后，两个Person的Age对象指向一个地址
         * 所以其中一个改变了Age对应的age值,两个Person的age值都会发生改变
         *
         *
         */
        Person person = new Person();
        person.setName("A");
        person.setAge(new Age(10));
        try {
            Person person1 = (Person) person.clone();
            System.out.println("复制后person-name为" + person.getName());
            System.out.println("复制后person1-name为" + person1.getName());
            System.out.println("复制后person-age为" + person.getAge());
            System.out.println("复制后person1-age为" + person1.getAge());
            person.setName("B");
            person.getAge().setAge(12);
            System.out.println("改变person的值后");
            System.out.println("改变person的值后person-name为" + person.getName());
            System.out.println("改变person的值后person1-name为" + person1.getName());
            System.out.println("复制后person-age为" + person.getAge().getAge());
            System.out.println("复制后person1-age为" + person1.getAge().getAge());

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        System.out.println("\n");
        System.out.println("\n");
        System.out.println("------------深复制--------------");
        /**
         * 浅复制相当于只复制了最上层的对象Person
         * 而Person对象中的Age对象则没有复制
         * 深复制则是将Person对象中的Age对象也复制一份
         */
        Person person3 = new Person();
        person3.setName("C");
        person3.setAge(new Age(20));
        try {
            Person clonePerson = (Person) person3.createClone();
            System.out.println("复制后person3-name为" + person3.getName());
            System.out.println("复制后clonePerson-name为" + clonePerson.getName());
            System.out.println("复制后person3-age为" + person3.getAge());
            System.out.println("复制后clonePerson-age为" + clonePerson.getAge());
            person3.setName("D");
            person3.getAge().setAge(22);
            System.out.println("改变person3的值后");
            System.out.println("改变person3的值后person3-name为" + person3.getName());
            System.out.println("改变person3的值后clonePerson-name为" + clonePerson.getName());
            System.out.println("复制后person3-age为" + person3.getAge().getAge());
            System.out.println("复制后clonePerson-age为" + clonePerson.getAge().getAge());

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
