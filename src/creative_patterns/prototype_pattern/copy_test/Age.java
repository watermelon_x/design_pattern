package creative_patterns.prototype_pattern.copy_test;

/**
 * 年龄
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/3
 */
public class Age implements Cloneable {

    public Age(Integer age) {
        this.age = age;
    }

    private Integer age;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
