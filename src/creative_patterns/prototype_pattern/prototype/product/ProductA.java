package creative_patterns.prototype_pattern.prototype.product;

/**
 * 产品1
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/4
 */
public class ProductA implements Product {
    private String name;

    @Override
    public Product createClone() {
        Product clone = null;
        try {
            clone = (Product) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
