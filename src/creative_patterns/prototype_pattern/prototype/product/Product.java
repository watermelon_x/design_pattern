package creative_patterns.prototype_pattern.prototype.product;

/**
 * 可克隆对象接口
 *
 * @author watermelon
 * @email zfquan91@foxmail.com
 * @date 2019/7/4
 */
public interface Product extends Cloneable {

    /**
     * 克隆方法
     *
     * @return
     */
    abstract Product createClone();
}
