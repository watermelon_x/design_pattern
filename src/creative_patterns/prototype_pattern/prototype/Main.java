package creative_patterns.prototype_pattern.prototype;

import creative_patterns.prototype_pattern.prototype.product.ProductA;

/**
 * 原型模式（克隆）
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Main {
    public static void main(String[] args) {
        ProductA productA = new ProductA();
        productA.setName("A");
        System.out.println(productA.getName());

        ProductA productClone = (ProductA) productA.createClone();
        System.out.println(productClone.getName());


    }
}
