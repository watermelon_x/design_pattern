package structural_patterns.decorator_pattern;


import structural_patterns.decorator_pattern.shape.Circle;
import structural_patterns.decorator_pattern.shape.Shape;
import structural_patterns.decorator_pattern.shape.Square;

/**
 * 装饰器模式
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Main {

    public static void main(String[] args) {
        Shape circle = new Circle();
        circle.draw();

        Shape square = new Square();
        VioletShapeDecorator violetShapeDecorator = new VioletShapeDecorator(square);
        violetShapeDecorator.draw();

    }
}
