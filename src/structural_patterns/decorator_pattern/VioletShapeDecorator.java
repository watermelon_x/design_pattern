package structural_patterns.decorator_pattern;

import structural_patterns.decorator_pattern.shape.Shape;

/**
 * 紫色装形状装饰类
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class VioletShapeDecorator extends ShapeDecorator {

    public VioletShapeDecorator(Shape decorate) {
        super(decorate);
    }

    @Override
    public void draw() {
        decorate.draw();
        addViolet();
    }

    private void addViolet() {
        System.out.println("\tadd Violet");
    }
}
