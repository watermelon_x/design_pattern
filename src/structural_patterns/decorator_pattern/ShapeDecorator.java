package structural_patterns.decorator_pattern;


import structural_patterns.decorator_pattern.shape.Shape;

/**
 * 抽象装饰类
 *
 * @author watermelon
 * @date 2019/5/29
 */
public abstract class ShapeDecorator implements Shape {

    protected Shape decorate;

    public ShapeDecorator(Shape decorate) {
        this.decorate = decorate;
    }

    @Override
    public void draw() {
        decorate.draw();
    }
}
