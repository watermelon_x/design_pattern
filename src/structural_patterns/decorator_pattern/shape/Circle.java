package structural_patterns.decorator_pattern.shape;

/**
 * 圆
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("draw: Circle");
    }
}
