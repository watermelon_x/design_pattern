package structural_patterns.decorator_pattern.shape;

/**
 * 图形接口
 *
 * @author watermelon
 * @date 2019/5/29
 */
public interface Shape {
    void draw();
}
