package structural_patterns.decorator_pattern.shape;

/**
 * 正方形
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("draw: Square");
    }
}
