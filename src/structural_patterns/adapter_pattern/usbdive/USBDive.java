package structural_patterns.adapter_pattern.usbdive;

/**
 * u 盘
 *
 * @author watermelon
 * @date 2019/5/27
 */
public interface USBDive {

    //连接U盘的方法
    void connectUSBDive();
}
