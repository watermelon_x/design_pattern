package structural_patterns.adapter_pattern.usbdive;

import structural_patterns.adapter_pattern.mouse.Mouse;

/**
 * 鼠标适配器
 * 让mouse能通过USBDive的连接方法与computer连接
 * （不改变computer的情况下，让computer与mouse连接）
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class MouseAdapter implements USBDive {

    private Mouse mouse;

    public MouseAdapter(Mouse mouse) {
        this.mouse = mouse;
    }

    @Override
    public void connectUSBDive() {
        System.out.println("鼠标适配成功...");
        mouse.connectMouse();
    }
}
