package structural_patterns.adapter_pattern.usbdive;

/**
 * U 盘实现
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class USBDiveImpl implements USBDive {
    @Override
    public void connectUSBDive() {
        System.out.println("连接U盘成功...");
    }
}
