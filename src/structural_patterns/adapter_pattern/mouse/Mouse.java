package structural_patterns.adapter_pattern.mouse;

/**
 * 鼠标
 *
 * @author watermelon
 * @date 2019/5/27
 */
public interface Mouse {

    //连接鼠标的方法
    void connectMouse();
}
