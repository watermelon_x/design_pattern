package structural_patterns.adapter_pattern.mouse;

/**
 * 鼠标实现
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class MouseImpl implements Mouse {
    @Override
    public void connectMouse() {
        System.out.println("连接鼠标成功...");
    }
}
