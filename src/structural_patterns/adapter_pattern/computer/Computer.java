package structural_patterns.adapter_pattern.computer;

import structural_patterns.adapter_pattern.usbdive.USBDive;

/**
 * 电脑
 *
 * @author watermelon
 * @date 2019/5/27
 */
public interface Computer {
    //与U盘连接
    void connect(USBDive usbDive);
}
