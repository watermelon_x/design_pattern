package structural_patterns.adapter_pattern.computer;

import structural_patterns.adapter_pattern.usbdive.USBDive;

/**
 * 电脑实现
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class ComputerImpl implements Computer {
    @Override
    public void connect(USBDive usbDive) {
        usbDive.connectUSBDive();
    }
}
