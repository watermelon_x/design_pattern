package structural_patterns.adapter_pattern;

import structural_patterns.adapter_pattern.computer.Computer;
import structural_patterns.adapter_pattern.computer.ComputerImpl;
import structural_patterns.adapter_pattern.usbdive.USBDive;
import structural_patterns.adapter_pattern.usbdive.USBDiveImpl;

/**
 * 适配器模式
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Main {

    public static void main(String[] args) {
        Computer computer = new ComputerImpl();

        USBDive usbDive = new USBDiveImpl();
        computer.connect(usbDive);


//        Mouse mouse = new MouseImpl();
//        USBDive usbDive1 = new MouseAdapter(mouse);
//        computer.connect(usbDive1);
    }
}
