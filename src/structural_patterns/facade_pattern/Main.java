package structural_patterns.facade_pattern;

/**
 * 外观模式
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Main {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.run();

    }
}
