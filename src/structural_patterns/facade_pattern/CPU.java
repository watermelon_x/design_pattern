package structural_patterns.facade_pattern;

/**
 * cpu
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class CPU {
    public void run() {
        System.out.println("cpu run...");
    }
}
