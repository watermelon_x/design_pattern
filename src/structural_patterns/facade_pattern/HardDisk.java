package structural_patterns.facade_pattern;

/**
 * 硬盘
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class HardDisk {
    public void run() {
        System.out.println("HardDisk run...");
    }
}
