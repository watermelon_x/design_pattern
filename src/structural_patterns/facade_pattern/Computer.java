package structural_patterns.facade_pattern;

/**
 * 电脑
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Computer {
    //cpu
    private CPU cpu;

    //硬盘
    private HardDisk hardDisk;

    public Computer() {
        cpu = new CPU();
        hardDisk = new HardDisk();
    }

    public void run() {
        cpu.run();
        hardDisk.run();
        System.out.println("Computer run...");
    }
}
