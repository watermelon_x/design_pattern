package structural_patterns.proxy_pattern.static_proxy;

/**
 * 真实玩家
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class RealGamePlayer implements GamePlayer {

    //玩家姓名
    private String name;


    @Override
    public void login() {
        System.out.println("玩家" + this.getName() + "登录了游戏");
    }

    @Override
    public void killLittleSoldier() {
        System.out.println("玩家" + this.getName() + "杀死了50个小兵");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
