package structural_patterns.proxy_pattern.static_proxy;

/**
 * （游戏玩家）主体接口
 *
 * @author watermelon
 * @date 2019/6/3
 */
public interface GamePlayer {

    //登录游戏
    void login();

    //杀死小兵
    void killLittleSoldier();
}
