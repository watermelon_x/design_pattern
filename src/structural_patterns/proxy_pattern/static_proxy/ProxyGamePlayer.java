package structural_patterns.proxy_pattern.static_proxy;

/**
 * 代练玩家
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class ProxyGamePlayer implements GamePlayer {

    //被代练的玩家
    private RealGamePlayer realGamePlayer;


    //代练别人的号
    public ProxyGamePlayer(RealGamePlayer realGamePlayer) {
        this.realGamePlayer = realGamePlayer;
    }

    @Override
    public void login() {
        System.out.print("代练玩家操作-");
        realGamePlayer.login();
    }

    @Override
    public void killLittleSoldier() {
        System.out.print("代练玩家操作-");
        realGamePlayer.killLittleSoldier();
        System.out.println("+50个小兵");
    }
}
