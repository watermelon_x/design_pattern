package structural_patterns.proxy_pattern.dynamic_proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 动态代理处理器
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class DynamicProxyHandler implements InvocationHandler {

    private Object obj;

    public DynamicProxyHandler(Object obj) {
        this.obj = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

//        System.out.println("代练玩家操作开始-");
//        //所有方法执行前都会拦截
//        Object invoke = method.invoke(obj, args);
//        System.out.println("代练玩家操作结束-");
//        return invoke;
        //当执行方法login时拦截
        String methodName = method.getName();
        switch (methodName) {
            case "login":
                System.out.print("代练玩家操作开始-");
                Object invoke = method.invoke(obj, args);
                System.out.println("代练玩家操作结束-");
                return invoke;
            case "killLittleSoldier":
                System.out.print("代练玩家操作-");
                Object invoke2 = method.invoke(obj, args);
                System.out.println("+50 个小兵");
                return invoke2;
            default:
                return method.invoke(obj, args);
        }

    }
}
