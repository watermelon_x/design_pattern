package structural_patterns.proxy_pattern.cglib_proxy;


import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Cglib 代理
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class CglibProxy implements MethodInterceptor {

    private Object obj;

    public Object getInstance(Object obj) {
        this.obj = obj;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(obj.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        Object invoke = null;
        if (method.getName().equals("login")) {
            System.out.print("代练玩家操作-");
            //直接执行被代理类的方法
            invoke = method.invoke(obj, objects);
            //通过代理类执行被代理类方法（TODO 这里被代理类的name属性会丢失，暂未知）
//            invoke = methodProxy.invokeSuper(o, objects);
            System.out.println("代练玩家结束操作-");
        }
        if (method.getName().equals("killLittleSoldier")) {
            System.out.print("代练玩家操作-");
            invoke = method.invoke(obj, objects);
//            invoke = methodProxy.invokeSuper(o, objects);
            System.out.println("+50个小兵");
        }
        return invoke;
    }
}
