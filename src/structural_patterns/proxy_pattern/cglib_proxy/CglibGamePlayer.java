package structural_patterns.proxy_pattern.cglib_proxy;

/**
 * 真实玩家
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class CglibGamePlayer {

    //玩家姓名
    private String name;


    public void login() {
        System.out.println("玩家" + this.getName() + "登录了游戏");
    }

    public void killLittleSoldier() {
        System.out.println("玩家" + this.getName() + "杀死了50个小兵");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
