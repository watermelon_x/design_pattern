package structural_patterns.proxy_pattern;

import structural_patterns.proxy_pattern.cglib_proxy.CglibGamePlayer;
import structural_patterns.proxy_pattern.cglib_proxy.CglibProxy;
import structural_patterns.proxy_pattern.dynamic_proxy.DynamicProxyHandler;
import structural_patterns.proxy_pattern.static_proxy.GamePlayer;
import structural_patterns.proxy_pattern.static_proxy.ProxyGamePlayer;
import structural_patterns.proxy_pattern.static_proxy.RealGamePlayer;

import java.lang.reflect.Proxy;

/**
 * 代理模式
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class Main {

    public static void main(String[] args) {


        RealGamePlayer realGamePlayer = new RealGamePlayer();
        realGamePlayer.setName("龙傲天");
        realGamePlayer.login();
        realGamePlayer.killLittleSoldier();


        System.out.println("------静态代理-----\n");
        //静态代理
        GamePlayer proxyGamePlayer = new ProxyGamePlayer(realGamePlayer);
        proxyGamePlayer.login();
        proxyGamePlayer.killLittleSoldier();


        System.out.println("\n\n\n------动态代理-----\n");
        //动态代理
        GamePlayer gamePlayer = new RealGamePlayer();
        ((RealGamePlayer) gamePlayer).setName("海绵宝宝");
        GamePlayer proxyPlayer = (GamePlayer) Proxy.newProxyInstance(GamePlayer.class.getClassLoader(), new Class[]{GamePlayer.class}, new DynamicProxyHandler(gamePlayer));
        proxyPlayer.login();
        proxyPlayer.killLittleSoldier();


        System.out.println("\n\n\n------CGLIB代理-----\n");
        CglibGamePlayer cglibGamePlayer = new CglibGamePlayer();
        cglibGamePlayer.setName("艺术就是派大星");
        CglibProxy proxy = new CglibProxy();
        CglibGamePlayer proxyGglibGamePlayer1 = (CglibGamePlayer) proxy.getInstance(cglibGamePlayer);
        proxyGglibGamePlayer1.login();
        proxyGglibGamePlayer1.killLittleSoldier();

    }
}
