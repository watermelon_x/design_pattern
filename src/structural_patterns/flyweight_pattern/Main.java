package structural_patterns.flyweight_pattern;

import java.util.Random;

/**
 * 享元模式
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Main {
    private static Random random = new Random();

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            Circle circle = ShapeFactory.getCircle(getColor());
            circle.setRadius(getRadius());
            circle.draw();
        }


    }

    //获取随机半径
    public static Integer getRadius() {
        return random.nextInt(20) + 1;
    }

    //获取随机颜色
    public static String getColor() {
        String[] colors = new String[]{"red", "blue", "white", "block", "yellow"};
        return colors[random.nextInt(4)];
    }
}
