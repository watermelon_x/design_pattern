package structural_patterns.flyweight_pattern;

/**
 * 形状
 *
 * @author watermelon
 * @date 2019/6/3
 */
public interface Shape {
    //画图
    void draw();
}
