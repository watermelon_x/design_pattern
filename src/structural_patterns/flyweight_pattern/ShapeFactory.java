package structural_patterns.flyweight_pattern;

import java.util.HashMap;
import java.util.Map;

/**
 * 生产圆的工厂
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class ShapeFactory {
    private static Map<String, Shape> circleMap = new HashMap<>();

    //通过color获取圆
    public static Circle getCircle(String color) {
        Circle circle = null;
        circle = (Circle) circleMap.get(color);
        if (circle == null) {
            circle = new Circle();
            circle.setColor(color);
            circleMap.put(color, circle);
            System.out.println("创建了一个新的对象Circle,color=" + color);
        }
        return circle;
    }

}

