package structural_patterns.flyweight_pattern;

/**
 * 圆
 *
 * @author watermelon
 * @date 2019/6/3
 */
public class Circle implements Shape {

    //半径
    private Integer radius;


    //颜色
    private String color;

    @Override
    public void draw() {
        System.out.println("Circle[radius:" + radius + ",color:" + color + "]");
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
