package structural_patterns.bridge_pattern;

import structural_patterns.bridge_pattern.draw.VioletCircle;
import structural_patterns.bridge_pattern.draw.YellowCircle;
import structural_patterns.bridge_pattern.shape.Circle;
import structural_patterns.bridge_pattern.shape.Shape;

/**
 * 桥接模式
 *
 * @author watermelon
 * @date 2019/5/27
 */
public class Main {
    public static void main(String[] args) {
        Shape shape = new Circle(5, 5, 5, new YellowCircle());
        Shape shape1 = new Circle(5, 5, 5, new VioletCircle());

        shape.draw();
        shape1.draw();
    }
}
