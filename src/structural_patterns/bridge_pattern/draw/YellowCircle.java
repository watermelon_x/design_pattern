package structural_patterns.bridge_pattern.draw;

/**
 * 黄色的圆
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class YellowCircle implements DrawAPI {
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("draw YellowCircle:radius=" + radius + ",x=" + x + ",y=" + y);
    }
}
