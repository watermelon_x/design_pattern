package structural_patterns.bridge_pattern.draw;

/**
 * 紫色的圆
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class VioletCircle implements DrawAPI {
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.println("draw VioletCircle:radius=" + radius + ",x=" + x + ",y=" + y);
    }
}
