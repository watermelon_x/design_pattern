package structural_patterns.bridge_pattern.draw;

/**
 * 桥接实现接口（draw接口）
 *
 * @author watermelon
 * @date 2019/5/28
 */
public interface DrawAPI {
    //画一个圆
    void drawCircle(int radius, int x, int y);
}
