package structural_patterns.bridge_pattern.shape;

import structural_patterns.bridge_pattern.draw.DrawAPI;

/**
 * 形状
 *
 * @author watermelon
 * @date 2019/5/28
 */
public abstract class Shape {
    public DrawAPI drawAPI;

    public Shape(DrawAPI drawAPI) {
        this.drawAPI = drawAPI;
    }

    public abstract void draw();
}
