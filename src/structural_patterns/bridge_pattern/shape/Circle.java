package structural_patterns.bridge_pattern.shape;

import structural_patterns.bridge_pattern.draw.DrawAPI;

/**
 * 圆
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class Circle extends Shape {

    private int x;
    private int y;
    private int radius;

    public Circle(int x, int y, int radius, DrawAPI drawAPI) {
        super(drawAPI);
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    @Override
    public void draw() {
        drawAPI.drawCircle(radius, x, y);
    }
}
