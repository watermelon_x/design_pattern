package structural_patterns.filter_pattern;

import structural_patterns.filter_pattern.criteria.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 过滤器模式
 * 或标准模式（Criteria Pattern）
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class Main {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<Person>();
        persons.add(new Person("child", "男", 15));
        persons.add(new Person("child", "女", 11));
        persons.add(new Person("xiaoming", "男", 13));
        persons.add(new Person("lili", "女", 12));


        Criteria criteria = new CriteriaNameChild();
        List<Person> people = criteria.meetCriteria(persons);
        System.out.println(Arrays.toString(people.toArray()));

        Criteria criteria1 = new CriteriaSexMan();
        List<Person> people1 = criteria1.meetCriteria(persons);
        System.out.println("\n" + Arrays.toString(people1.toArray()));


        Criteria criteria2 = new AndCriteria(new CriteriaNameChild(), new CriteriaSexMan());
        List<Person> people2 = criteria2.meetCriteria(persons);
        System.out.println("\n" + Arrays.toString(people2.toArray()));

        Criteria criteria3 = new OrCriteria(new CriteriaNameChild(), new CriteriaSexMan());
        List<Person> people3 = criteria3.meetCriteria(persons);
        System.out.println("\n" + Arrays.toString(people3.toArray()));
    }
}
