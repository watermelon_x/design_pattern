package structural_patterns.filter_pattern;

/**
 * 人（被过滤兑现）
 *
 * @author watermelon
 * @date 2019/5/28
 */

public class Person {

    public Person(String name, String sex, Integer age) {
        this.name = name;
        this.sex = sex;
        this.age = age;
    }

    //姓名
    private String name;

    //性别
    private String sex;

    //年龄
    private Integer age;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "[name:" + this.getName() + ",sex:" + this.getSex() + ",age:" + this.getAge() + "]";
    }
}
