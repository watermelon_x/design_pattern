package structural_patterns.filter_pattern.criteria;

import structural_patterns.filter_pattern.Person;

import java.util.List;

/**
 * and连接
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class AndCriteria implements Criteria {

    private Criteria criteria;
    private Criteria otherCriteria;


    public AndCriteria(Criteria criteria, Criteria otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> people = criteria.meetCriteria(persons);
        return otherCriteria.meetCriteria(people);
    }
}
