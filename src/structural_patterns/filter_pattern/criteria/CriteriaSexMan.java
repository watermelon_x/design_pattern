package structural_patterns.filter_pattern.criteria;

import structural_patterns.filter_pattern.Person;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 过滤性别为男
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class CriteriaSexMan implements Criteria {
    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> returnList = persons.stream().filter(person -> "男".equals(person.getSex())).collect(Collectors.toList());
        return returnList;
    }
}
