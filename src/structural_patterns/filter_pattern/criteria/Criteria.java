package structural_patterns.filter_pattern.criteria;

import structural_patterns.filter_pattern.Person;

import java.util.List;

/**
 * 过滤器接口
 *
 * @author watermelon
 * @date 2019/5/28
 */
public interface Criteria {
    List<Person> meetCriteria(List<Person> persons);
}
