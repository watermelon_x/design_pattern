package structural_patterns.filter_pattern.criteria;

import structural_patterns.filter_pattern.Person;

import java.util.List;
import java.util.stream.Collectors;

/**
 * or连接
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class OrCriteria implements Criteria {
    private Criteria criteria;
    private Criteria otherCriteria;

    public OrCriteria(Criteria criteria, Criteria otherCriteria) {
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> people = criteria.meetCriteria(persons);
        List<Person> people1 = otherCriteria.meetCriteria(persons);
        for (Person p : people) {
            if (!people1.contains(p)) {
                people1.add(p);
            }
        }
        return people1;
    }
}
