package structural_patterns.filter_pattern.criteria;

import structural_patterns.filter_pattern.Person;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 过滤姓名为child
 *
 * @author watermelon
 * @date 2019/5/28
 */
public class CriteriaNameChild implements Criteria {
    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> collect = persons.stream().filter(person -> "child".equals(person.getName())).collect(Collectors.toList());
        return collect;
    }
}
