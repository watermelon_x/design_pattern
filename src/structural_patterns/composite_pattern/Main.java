package structural_patterns.composite_pattern;

/**
 * 组合模式
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Main {

    public static void main(String[] args) {
        //ceo
        Employee CEO = new Employee("ceo", "CEO", 30000);
        //销售老大
        Employee headSales = new Employee("headSales", "Head Sales", 15000);

        //营销老大
        Employee headMarketing = new Employee("headMarketing", "Head Marketing", 15000);

        //职员
        Employee clerk1 = new Employee("clerk1", "Marketing", 5000);
        Employee clerk2 = new Employee("clerk2", "Marketing", 5000);

        //销售人员
        Employee salesExecutive1 = new Employee("salesExecutive1", "Sales", 5000);
        Employee salesExecutive2 = new Employee("salesExecutive2", "Sales", 5000);


        CEO.addSub(headSales);
        CEO.addSub(headMarketing);

        headSales.addSub(salesExecutive1);
        headSales.addSub(salesExecutive2);

        headMarketing.addSub(clerk1);
        headMarketing.addSub(clerk2);

        CEO.organizationStructure();
        headMarketing.organizationStructure();
        headSales.organizationStructure();

    }
}
