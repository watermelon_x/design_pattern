package structural_patterns.composite_pattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 员工
 *
 * @author watermelon
 * @date 2019/5/29
 */
public class Employee {
    //姓名
    private String name;
    //部门
    private String dept;
    //工资
    private Integer wages;
    //下级员工
    private List<Employee> subordinate;

    public Employee(String name, String dept, Integer wages) {
        this.name = name;
        this.dept = dept;
        this.wages = wages;
        subordinate = new ArrayList<>();
    }

    //增加一个下级员工
    public void addSub(Employee employee) {
        subordinate.add(employee);
    }

    //移除一个下级员工
    public void removeSub(Employee employee) {
        subordinate.remove(employee);
    }

    //打印当前组织结构
    public void organizationStructure() {
        System.out.println(this.toString());
        forSout(this.getSubordinate());
    }

    //循环输出下级
    private void forSout(List<Employee> subordinates) {
        for (Employee e : subordinates) {
            System.out.println(e);
            if (e.getSubordinate().size() > 0) {
                forSout(e.getSubordinate());
            }
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Integer getWages() {
        return wages;
    }

    public void setWages(Integer wages) {
        this.wages = wages;
    }

    public List<Employee> getSubordinate() {
        return subordinate;
    }

    public void setSubordinate(List<Employee> subordinate) {
        this.subordinate = subordinate;
    }

    @Override
    public String toString() {
        return "Employee [name:" + this.getName() + ",dept:" + this.getDept() + ",wages:" + getWages() + "]";
    }


}
